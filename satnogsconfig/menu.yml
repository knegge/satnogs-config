---
_submenu_description: &submenu_description |
  [UP], [DOWN] arrow keys to move
  [ENTER] to select

type: 'submenu'
short_description: 'Main menu'
description: *submenu_description
exit_button: true
cancel_label: 'Exit'
cancel:
  type: 'exit'
  title: 'Basic configuration incomplete'
  short_description: >-
    WARNING: Some 'Basic' configuration options are not set.
    All basic options should be set for a functional SatNOGS station.
    \n\nContinue with partial configuration?
  defaultno: true
esc:
  type: 'exit'
  title: 'Basic configuration incomplete'
  short_description: >-
    WARNING: Some 'Basic' configuration options are not set.
    All basic options should be set for a functional SatNOGS station.
    \n\nContinue with partial configuration?
  defaultno: true
defaults:
  ok_label: 'Select'
  cancel_label: 'Back'
  exit_label: 'Exit'
  help_label: 'Help'
  no_label: 'No'
  yes_label: 'Yes'
items:
  Basic:
    type: 'submenu'
    short_description: 'Basic configuration options'
    title: 'Basic configuration options'
    description: *submenu_description
    ok_label: 'Select'
    cancel_label: 'Back'
    defaults:
      title: 'Parameter definition'
      ok_label: 'Save'
      cancel_label: 'Cancel'
    items:
      SATNOGS_API_TOKEN:
        type: 'variablebox'
        short_description: 'Define API token'
        variable: 'satnogs_api_token'
        mandatory: true
      SATNOGS_SOAPY_RX_DEVICE:
        type: 'variablebox'
        short_description: 'Define Soapy RX device'
        variable: 'satnogs_soapy_rx_device'
        mandatory: true
      SATNOGS_ANTENNA:
        type: 'variablebox'
        short_description: 'Define SatNOGS Radio Antenna'
        variable: 'satnogs_antenna'
        mandatory: true
      SATNOGS_RX_SAMP_RATE:
        type: 'variablebox'
        short_description: 'Define RX sampling rate'
        variable: 'satnogs_rx_samp_rate'
        mandatory: true
      SATNOGS_STATION_ELEV:
        type: 'variablebox'
        short_description: 'Define station elevation'
        variable: 'satnogs_station_elev'
        mandatory: true
      SATNOGS_STATION_ID:
        type: 'variablebox'
        short_description: 'Define station ID'
        variable: 'satnogs_station_id'
        mandatory: true
      SATNOGS_STATION_LAT:
        type: 'variablebox'
        short_description: 'Define station latitude'
        variable: 'satnogs_station_lat'
        mandatory: true
      SATNOGS_STATION_LON:
        type: 'variablebox'
        short_description: 'Define station longitude'
        variable: 'satnogs_station_lon'
        mandatory: true
  Advanced:
    type: 'submenu'
    short_description: 'Advanced configuration options'
    description: *submenu_description
    defaults:
      ok_label: 'Select'
      cancel_label: 'Back'
    items:
      Network:
        type: 'submenu'
        short_description: 'Network settings'
        title: 'Network settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          SATNOGS_API_TOKEN:
            type: 'variablebox'
            short_description: 'Define API token'
            variable: 'satnogs_api_token'
          SATNOGS_STATION_ELEV:
            type: 'variablebox'
            short_description: 'Define station elevation'
            variable: 'satnogs_station_elev'
          SATNOGS_STATION_ID:
            type: 'variablebox'
            short_description: 'Define station ID'
            variable: 'satnogs_station_id'
          SATNOGS_STATION_LAT:
            type: 'variablebox'
            short_description: 'Define station latitude'
            variable: 'satnogs_station_lat'
          SATNOGS_STATION_LON:
            type: 'variablebox'
            short_description: 'Define station longitude'
            variable: 'satnogs_station_lon'
          SATNOGS_NETWORK_API_URL:
            type: 'variablebox'
            short_description: 'Define network API URL'
            variable: 'satnogs_network_api_url'
            init: 'https://network.satnogs.org/api/'
          SATNOGS_NETWORK_API_QUERY_INTERVAL:
            type: 'variablebox'
            short_description: 'Define network API query interval'
            variable: 'satnogs_network_api_query_interval'
            init: '60'
          SATNOGS_NETWORK_API_POST_INTERVAL:
            type: 'variablebox'
            short_description: 'Define network API post interval'
            variable: 'satnogs_network_api_post_interval'
            init: '180'
          SATNOGS_VERIFY_SSL:
            type: 'variableyesno'
            short_description: 'Verify SatNOGS network SSL certificate'
            variable: 'satnogs_verify_ssl'
            defaultno: false
      Radio:
        type: 'submenu'
        short_description: 'Radio settings'
        title: 'Radio settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          SATNOGS_RIG_IP:
            type: 'variablebox'
            short_description: 'Define Hamlib rigctld IP'
            variable: 'satnogs_rig_ip'
            init: 'rigctld'
          SATNOGS_RIG_PORT:
            type: 'variablebox'
            short_description: 'Define Hamlib rigctld port'
            variable: 'satnogs_rig_port'
            init: '4532'
          SATNOGS_DOPPLER_CORR_PER_SEC:
            type: 'variablebox'
            short_description: 'Define doppler corrections per sec'
            variable: 'satnogs_doppler_corr_per_sec'
          SATNOGS_PPM_ERROR:
            type: 'variablebox'
            short_description: 'Define frequency correction (ppm)'
            variable: 'satnogs_ppm_error'
          SATNOGS_LO_OFFSET:
            type: 'variablebox'
            short_description: 'Define local oscillator offset (Hz)'
            variable: 'satnogs_lo_offset'
          SATNOGS_GAIN_MODE:
            type: 'variablebox'
            short_description: |-
              Define SatNOGS Radio Gain mode (Overall, Settings Field)
            variable: 'satnogs_gain_mode'
            init: 'Overall'
          SATNOGS_RF_GAIN:
            type: 'variablebox'
            short_description: 'Define SatNOGS Radio RF Gain'
            variable: 'satnogs_rf_gain'
          SATNOGS_RX_BANDWIDTH:
            type: 'variablebox'
            short_description: 'Define SatNOGS Radio RF Bandwidth (Hz)'
            variable: 'satnogs_rx_bandwidth'
          SATNOGS_ANTENNA:
            type: 'variablebox'
            short_description: 'Define SatNOGS Radio Antenna'
            variable: 'satnogs_antenna'
          SATNOGS_SOAPY_RX_DEVICE:
            type: 'variablebox'
            short_description: 'Define Soapy RX device'
            variable: 'satnogs_soapy_rx_device'
          SATNOGS_RX_SAMP_RATE:
            type: 'variablebox'
            short_description: 'Define RX sampling rate'
            variable: 'satnogs_rx_samp_rate'
          SATNOGS_DEV_ARGS:
            type: 'variablebox'
            short_description: 'Define SatNOGS Radio device arguments'
            variable: 'satnogs_dev_args'
          SATNOGS_STREAM_ARGS:
            type: 'variablebox'
            short_description: 'Define SatNOGS Radio stream arguments'
            variable: 'satnogs_stream_args'
          SATNOGS_TUNE_ARGS:
            type: 'variablebox'
            short_description: 'Define SatNOGS Radio tune arguments'
            variable: 'satnogs_tune_args'
          SATNOGS_OTHER_SETTINGS:
            type: 'variablebox'
            short_description: |-
              Define SatNOGS Radio other settings field options
            variable: 'satnogs_other_settings'
          SATNOGS_DC_REMOVAL:
            type: 'variableyesno'
            short_description: 'Enable automatic DC removal'
            variable: 'satnogs_dc_removal'
            defaultno: false
          SATNOGS_BB_FREQ:
            type: 'variablebox'
            short_description: |-
              Define SatNOGS Radio baseband frequency correction
            variable: 'satnogs_bb_freq'
          ENABLE_IQ_DUMP:
            type: 'variableyesno'
            short_description: 'Enable IQ dump'
            variable: 'enable_iq_dump'
            defaultno: true
          IQ_DUMP_FILENAME:
            type: 'variablebox'
            short_description: 'Define IQ dump filename'
            variable: 'iq_dump_filename'
          DISABLE_DECODED_DATA:
            type: 'variableyesno'
            short_description: 'Disable decoded data'
            variable: 'disable_decoded_data'
            defaultno: true
          UDP_DUMP_HOST:
            type: 'variablebox'
            short_description: 'Destination host for UDP sink'
            variable: 'udp_dump_host'
          UDP_DUMP_PORT:
            type: 'variablebox'
            short_description: 'Destination port for UDP sink'
            variable: 'udp_dump_port'
      Rotator:
        type: 'submenu'
        short_description: 'Rotator settings'
        title: 'Rotator settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          SATNOGS_ROT_ENABLED:
            type: 'variableyesno'
            short_description: 'Enable use of antenna rotator'
            variable: 'satnogs_rot_enabled'
            defaultno: true
          SATNOGS_ROT_MODEL:
            type: 'variablebox'
            short_description: 'Define Hamlib rotator model'
            variable: 'satnogs_rot_model'
            init: 'ROT_MODEL_DUMMY'
          SATNOGS_ROT_BAUD:
            type: 'variablebox'
            short_description: 'Define Hamlib rotator baud rate'
            variable: 'satnogs_rot_baud'
            init: '19200'
          SATNOGS_ROT_PORT:
            type: 'variablebox'
            short_description: 'Define Hamlib rotctld port'
            variable: 'satnogs_rot_port'
            init: '/dev/ttyUSB0'
          SATNOGS_ROT_THRESHOLD:
            type: 'variablebox'
            short_description: 'Define Hamlib rotator command threshold'
            variable: 'satnogs_rot_threshold'
            init: '4'
          SATNOGS_ROT_FLIP:
            type: 'variableyesno'
            short_description: 'Enable Hamlib rotator flip'
            variable: 'satnogs_rot_flip'
            defaultno: true
          SATNOGS_ROT_FLIP_ANGLE:
            type: 'variablebox'
            short_description: 'Define Hamlib rotator flip angle'
            variable: 'satnogs_rot_flip_angle'
            init: '75'
      Waterfall:
        type: 'submenu'
        short_description: 'Waterfall settings'
        title: 'Waterfall settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          SATNOGS_WATERFALL_AUTORANGE:
            type: 'variableyesno'
            short_description: 'Enable waterfall power range adjustment'
            variable: 'satnogs_waterfall_autorange'
            defaultno: false
          SATNOGS_WATERFALL_MIN_VALUE:
            type: 'variablebox'
            short_description: 'Define waterfall minimum power'
            variable: 'satnogs_waterfall_min_value'
            init: '-100'
          SATNOGS_WATERFALL_MAX_VALUE:
            type: 'variablebox'
            short_description: 'Define Waterfall maximum power'
            variable: 'satnogs_waterfall_max_value'
            init: '-50'
      Artifacts:
        type: 'submenu'
        short_description: 'Artifacts settings'
        title: 'Artifacts settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          SATNOGS_ARTIFACTS_ENABLED:
            type: 'variableyesno'
            short_description: 'Enable uploading of artifacts'
            variable: 'satnogs_artifacts_enabled'
            defaultno: true
          SATNOGS_ARTIFACTS_API_URL:
            type: 'variablebox'
            short_description: 'Define artifacts server API URL'
            variable: 'satnogs_artifacts_api_url'
            init: 'https://db.satnogs.org/api/'
          SATNOGS_ARTIFACTS_API_POST_INTERVAL:
            type: 'variablebox'
            short_description: 'Define artifacts server API post interval'
            variable: 'satnogs_artifacts_api_post_interval'
            init: '180'
          SATNOGS_ARTIFACTS_API_TOKEN:
            type: 'variablebox'
            short_description: 'Define artifacts server API token'
            variable: 'satnogs_artifacts_api_token'
          SATNOGS_KEEP_ARTIFACTS:
            type: 'variableyesno'
            short_description: 'Keep artifact files'
            variable: 'satnogs_keep_artifacts'
            defaultno: true
          SATNOGS_ARTIFACTS_OUTPUT_PATH:
            type: 'variablebox'
            short_description: 'Define path for storing artifact files'
            variable: 'satnogs_artifacts_output_path'
            init: '/tmp/.satnogs/artifacts'
      Uploads:
        type: 'submenu'
        short_description: 'Upload settings'
        title: 'Upload settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          SATNOGS_UPLOAD_AUDIO_FILES:
            type: 'variableyesno'
            short_description: 'Enable uploading audio to SatNOGS network.'
            variable: 'satnogs_upload_audio_files'
            defaultno: false
          SATNOGS_UPLOAD_WATERFALL_FILES:
            type: 'variableyesno'
            short_description: 'Enable uploading waterfalls to SatNOGS network.'
            variable: 'satnogs_upload_waterfall_files'
            defaultno: false
      Scripts:
        type: 'submenu'
        short_description: 'Pre/post-observation scripts'
        title: 'Pre/post-observation scripts'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          SATNOGS_PRE_OBSERVATION_SCRIPT:
            type: 'variablebox'
            short_description: 'Define pre-observation script'
            variable: 'satnogs_pre_observation_script'
          SATNOGS_POST_OBSERVATION_SCRIPT:
            type: 'variablebox'
            short_description: 'Define post-observation script'
            variable: 'satnogs_post_observation_script'
      Paths:
        type: 'submenu'
        short_description: 'Path settings'
        title: 'Path settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          SATNOGS_APP_PATH:
            type: 'variablebox'
            short_description: 'Define application data path'
            variable: 'satnogs_app_path'
            init: '/tmp/.satnogs'
          SATNOGS_OUTPUT_PATH:
            type: 'variablebox'
            short_description: 'Define output data path'
            variable: 'satnogs_output_path'
            init: '/tmp/.satnogs/data'
          SATNOGS_COMPLETE_OUTPUT_PATH:
            type: 'variablebox'
            short_description: 'Define completed data path'
            variable: 'satnogs_complete_output_path'
          SATNOGS_INCOMPLETE_OUTPUT_PATH:
            type: 'variablebox'
            short_description: 'Define incompleted data path'
            variable: 'satnogs_incomplete_output_path'
            init: '/tmp/.satnogs/data/incomplete'
          SATNOGS_REMOVE_RAW_FILES:
            type: 'variableyesno'
            short_description: 'Remove raw files'
            variable: 'satnogs_remove_raw_files'
            defaultno: false
          SATNOGS_KEEP_ARTIFACTS:
            type: 'variableyesno'
            short_description: 'Keep artifact files'
            variable: 'satnogs_keep_artifacts'
            defaultno: false
          SATNOGS_ARTIFACTS_OUTPUT_PATH:
            type: 'variablebox'
            short_description: 'Define path for storing artifact files'
            variable: 'satnogs_artifacts_output_path'
            init: '/tmp/.satnogs/artifacts'
      Hamlib:
        type: 'submenu'
        short_description: 'Hamlib settings'
        title: 'Hamlib settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          HAMLIB_UTILS_ROT_ENABLED:
            type: 'variableyesno'
            short_description: 'Enable Hamlib rotctld'
            variable: 'hamlib_utils_rot_enabled'
            defaultno: true
          HAMLIB_UTILS_ROT_OPTS:
            type: 'variablebox'
            short_description: 'Define Hamlib rotctld options'
            variable: 'hamlib_utils_rot_opts'
          HAMLIB_UTILS_RIG_DISABLED:
            type: 'variableyesno'
            short_description: 'Disable Hamlib rigctld'
            variable: 'hamlib_utils_rig_disabled'
            defaultno: true
          HAMLIB_UTILS_RIG_OPTS:
            type: 'variablebox'
            short_description: 'Define Hamlib rigctld options'
            variable: 'hamlib_utils_rig_opts'
      SNMP:
        type: 'submenu'
        short_description: 'SNMP settings'
        title: 'SNMP settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          SNMPD_ENABLED:
            type: 'variableyesno'
            short_description: 'Enable snmpd'
            variable: 'snmpd_enabled'
            defaultno: true
          SNMPD_AGENTADDRESS:
            type: 'variablebox'
            short_description: 'Define snmpd agentAddress'
            variable: 'snmpd_agentaddress'
          SNMPD_ROCOMMUNITY:
            type: 'variablebox'
            short_description: 'Define snmpd rocommunity'
            variable: 'snmpd_rocommunity'
      GPS:
        type: 'submenu'
        short_description: 'GPS settings'
        title: 'GPS settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          GPSD_ENABLED:
            type: 'variableyesno'
            short_description: 'Enable GPSd'
            variable: 'gpsd_enabled'
            defaultno: true
          SATNOGS_GPSD_CLIENT_ENABLED:
            type: 'variableyesno'
            short_description: 'Enable GPSd client'
            variable: 'satnogs_gpsd_client_enabled'
            defaultno: true
          SATNOGS_GPSD_HOST:
            type: 'variablebox'
            short_description: 'GPSd host'
            variable: 'satnogs_gpsd_host'
            init: '127.0.0.1'
          SATNOGS_GPSD_PORT:
            type: 'variablebox'
            short_description: 'GPSd port'
            variable: 'satnogs_gpsd_port'
            init: '2947'
          SATNOGS_GPSD_TIMEOUT:
            type: 'variablebox'
            short_description: 'GPSd timeout'
            variable: 'satnogs_gpsd_timeout'
            init: '30'
      Development:
        type: 'submenu'
        short_description: 'Development settings'
        title: 'Development settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          EXPERIMENTAL:
            type: 'variableyesno'
            short_description: 'Install latest versions of all software'
            variable: 'experimental'
            defaultno: true
          SATNOGS_SETUP_SATNOGS_ANSIBLE_CHECKOUT_REF:
            type: 'variablebox'
            short_description: 'Define SatNOGS Ansible Git branch/tag/commit'
            variable: 'satnogs_setup_satnogs_ansible_checkout_ref'
          SATNOGS_SETUP_SATNOGS_ANSIBLE_PLAYBOOK_URL:
            type: 'variablebox'
            short_description: 'Define SatNOGS Ansible Git repository URL'
            variable: 'satnogs_setup_satnogs_ansible_playbook_url'
          SATNOGS_SETUP_DOCKER_ANSIBLE_IMAGE:
            type: 'variablebox'
            short_description: 'Define SatNOGS Ansible Docker image'
            variable: 'satnogs_setup_docker_ansible_image'
          SATNOGS_SETUP_DOCKER_SATNOGS_CONFIG_IMAGE:
            type: 'variablebox'
            short_description: 'Define SatNOGS Config Docker image'
            variable: 'satnogs_setup_docker_satnogs_config_image'
          DOCKER_SATNOGS_CLIENT_IMAGE:
            type: 'variablebox'
            short_description: 'Define SatNOGS Client Docker image'
            variable: 'docker_satnogs_client_image'
          DOCKER_RIGCTLD_IMAGE:
            type: 'variablebox'
            short_description: 'Define rigctld Hamlib Docker image'
            variable: 'docker_rigctld_image'
          DOCKER_ROTCTLD_IMAGE:
            type: 'variablebox'
            short_description: 'Define rotctld Hamlib Docker image'
            variable: 'docker_rotctld_image'
      Debug:
        type: 'submenu'
        short_description: 'Debug settings'
        title: 'Debug settings'
        description: *submenu_description
        ok_label: 'Select'
        cancel_label: 'Back'
        defaults:
          title: 'Parameter definition'
          ok_label: 'Save'
          cancel_label: 'Cancel'
        items:
          SATNOGS_LOG_LEVEL:
            type: 'variablebox'
            short_description: 'Define SatNOGS client log level'
            variable: 'satnogs_log_level'
            init: 'WARNING'
          SATNOGS_SCHEDULER_LOG_LEVEL:
            type: 'variablebox'
            short_description: 'Define SatNOGS client scheduler log level'
            variable: 'satnogs_scheduler_log_level'
            init: 'WARNING'
          SENTRY_DSN:
            type: 'variablebox'
            short_description: 'Define SatNOGS client Sentry DSN'
            variable: 'sentry_dsn'
            init: 'd50342fb75aa8f3945e2f846b77a0cdb7c7d2275'
          SENTRY_ENABLED:
            type: 'variableyesno'
            short_description: 'Enable SatNOGS client Sentry error monitoring'
            variable: 'sentry_enabled'
            defaultno: true
      Support:
        type: 'support'
        short_description: 'Generate support information'
  Show:
    type: 'configbox'
    short_description: 'Show configuration file'
    title: 'SatNOGS client configuration'
    exit_label: 'Back'
  Reset:
    type: 'resetyesno'
    short_description: 'Reset configuration'
    defaultno: true
  About:
    type: 'msgbox'
    short_description: 'Information about satnogs-config'
    title: 'SatNOGS config utility'
    message: 'satnogs-config is a tool for configuring SatNOGS client system'
    ok_label: 'Back'
